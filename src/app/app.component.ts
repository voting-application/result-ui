import { Component } from '@angular/core';
import io from 'socket.io-client';

// Helper function calculating vote repartition
const getPercentages = (a: number, b: number) => {
  const result = {
    a: 0,
    b: 0,
  };

  if (a + b > 0) {
    result.a = Math.round((a / (a + b)) * 100);
    result.b = 100 - result.a;
  } else {
    result.b = 50;
    result.a = result.b;
  }

  return result;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  private socket: any;

  public socketMessage = '';

  public opacity = '';

  public aPercent = '50%';

  public bPercent = '50%';

  public total = 0;

  // Credits image
  public creditsLink: string | null = '';
  public creditsImageUrl: string | null = '';
  public isCreditsImageValid = true;

  // Items images
  public imageAUrl: string | null = '';
  public imageBUrl: string | null = ''; 
  public isImageAValid = true;
  public isImageBValid = true;

  // Labels with default values
  public labelA: string = 'Cats';
  public labelB: string = 'Dogs';

  constructor() {
    this.socket = io('', {
      path: '/socket.io',
    });

    // Fetch config.json and set the image URLs dynamically
    this.loadConfig();

    // Change something on the UI to indicate new message received
    this.socket.on('message', ({ text }: { text: string }) => {
      this.socketMessage = text;
      this.opacity = '0.6';
    });

    // Update scores when a new vote is received
    this.socket.on('scores', (json: any) => {
      this.opacity = '1';

      // Extract number of votes for each item (a / b)
      const data: { a: number, b: number } = JSON.parse(json);
      const a = parseInt(String(data.a || 0), 10);
      const b = parseInt(String(data.b || 0), 10);

      // Get percentage of votes for each item
      const percentages = getPercentages(a, b);

      // Setting those props that should be used to move left/right the vertical slider on the UI
      this.aPercent = `${percentages.a}%`;
      this.bPercent = `${percentages.b}%`;

      // Setting this prop to indicate the total number of votes
      this.total = a + b;
    });
  }

  loadConfig(): void {
    fetch('/config/config.json')
      .then(response => response.json())
      .then((config) => {
        console.log('Config loaded:', config);
        if (config.images) {
          this.imageAUrl = config.images.a || '';
          this.imageBUrl = config.images.b || '';
          this.checkImageExists(this.imageAUrl, 'item-a');
          this.checkImageExists(this.imageBUrl, 'item-b');
        } else {
          console.log("image URLs not provided");
        }

        if (config.labels) {
          // Add type checking and conversion if needed
          this.labelA = String(config.labels.a || this.labelA);
          this.labelB = String(config.labels.b || this.labelB);
        } else {
          console.log("Labels not provided");
        }

        if (config.credits.imageURL) {
          this.creditsImageUrl = config.credits.imageURL || '';
          this.creditsLink = config.credits.link || '';
          this.checkImageExists(this.creditsImageUrl, 'credits');
        } else {
          console.log("Credits image not provided");
        }

        console.log('Values (after setting):', {
          labelA: this.labelA,
          labelB: this.labelB,
          imageAUrl: this.imageAUrl,
          imageBUrl: this.imageBUrl,
          creditsImageUrl: this.creditsImageUrl,
          creditsLink: this.creditsLink
        });
      })
      .catch(error => {
        console.error('Error loading config:', error);
        console.log('Values that will be used:', {
          labelA: this.labelA,
          labelB: this.labelB,
          imageAUrl: this.imageAUrl,
          imageBUrl: this.imageBUrl,
          creditsImageUrl: this.creditsImageUrl,
          creditsLink: this.creditsLink
        });
      });
  }

  // Check if an image exists using <img> tag
  checkImageExists(url: string | null, type: 'item-a' | 'item-b' | 'credits'): void {
    if (!url) return;

    const img = new Image();

    img.onload = () => {
      console.log(`${type} image exists`);
    };

    img.onerror = () => {
      console.log(`${type} image => error loading`);
      this.onImageError(type);
    };

    img.src = url;
  }

  // Handle image loading error
  onImageError(type: 'item-a' | 'item-b' | 'credits'): void {
    if (type === 'item-a') {
      this.isImageAValid = false;
    } else if (type === 'item-b') {
      this.isImageBValid = false;
    } else if (type === 'credits') {
      this.isCreditsImageValid =  false;
    }
  }
}
